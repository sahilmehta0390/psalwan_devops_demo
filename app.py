from flask import Flask, render_template

import redis
import os
redis_hostName = os.environ['redis_host']
r = redis.Redis(host=redis_hostName)

tpl_dir = 'templates/'
app = Flask(__name__, template_folder=tpl_dir)


@app.route("/")
def hello():
    import random
    import datetime
    dict_region_latencies = {
        'australia-southeast': random.randrange(5,8),
        'asia-east	' : random.randrange(8,11),
        'asia-northeast' : random.randrange(8,11),
        'asia-south' : random.randrange(8,11),
        'asia-southeast': random.randrange(8,11),
        'europe-north': random.randrange(15,25),
        'northamerica-northeast': random.randrange(17,25),
        'southamerica-east': random.randrange(17,25),
        'us-central': random.randrange(15,26),
        'us-east': random.randrange(16,26),
        'us-west': random.randrange(16,26)
    }
    return render_template('index.html', dict_bag = dict_region_latencies, curr_date=datetime.datetime.now(), counter=perform_redis_operation())


def perform_redis_operation():
    try:
        r.set('local_call', int(r.get('local_call')) + 1)
    except:
        r.set('local_call', 1)

    return bytes(r.get('local_call')).decode('utf-8')

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)


